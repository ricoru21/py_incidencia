-- MySQL dump 10.13  Distrib 5.6.28, for Linux (x86_64)
--
-- Host: localhost    Database: bdincidencia
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `idarea` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `estado` char(1) NOT NULL,
  `idPlanta` int(11) NOT NULL,
  PRIMARY KEY (`idarea`),
  KEY `fk_Area_Planta1_idx` (`idPlanta`),
  CONSTRAINT `fk_Area_Planta1` FOREIGN KEY (`idPlanta`) REFERENCES `planta` (`idplanta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Contabilidad','1',1);
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `idcargo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `estado` char(1) NOT NULL,
  PRIMARY KEY (`idcargo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` VALUES (1,'Secretario(a)','1'),(2,'Contador(a)','1');
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `idempleado` int(11) NOT NULL AUTO_INCREMENT,
  `keycode` char(4) NOT NULL,
  `dni` char(8) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `fechanacimiento` date NOT NULL,
  `estado` char(1) NOT NULL,
  `anexo` varchar(12) DEFAULT NULL,
  `correo` varchar(250) DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `idcargo` int(11) NOT NULL,
  `idarea` int(11) NOT NULL,
  PRIMARY KEY (`idempleado`),
  KEY `fk_empleado_Cargo_idx` (`idcargo`),
  KEY `fk_empleado_Area1_idx` (`idarea`),
  CONSTRAINT `fk_empleado_Area` FOREIGN KEY (`idarea`) REFERENCES `area` (`idarea`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_empleado_Cargo` FOREIGN KEY (`idcargo`) REFERENCES `cargo` (`idcargo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'0001','98672309','TestEjemplo','Prueba','2015-09-01','1','034234','testejemplo@gmail.com',NULL,1,1),(2,'0002','47197204','richard','oruna ','1991-07-03','1','','ricoru21@gmail.com',NULL,1,1);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incidencia`
--

DROP TABLE IF EXISTS `incidencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incidencia` (
  `idincidencia` int(11) NOT NULL AUTO_INCREMENT,
  `keycode` char(6) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `tipoprioridad` varchar(50) DEFAULT NULL,
  `tiemporesolucion` varchar(20) DEFAULT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `asginadoa` varchar(45) NOT NULL,
  `empleadoasignado` int(11) NOT NULL COMMENT 'este es el empleado a quien fue asignada la incidencia.',
  `tipo` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `descripcion` text NOT NULL,
  `idempleado` int(11) NOT NULL COMMENT 'el empleado que solicita la incidencia.',
  `usuariocrea` int(11) NOT NULL,
  `fechaIncidencia` date NOT NULL,
  `fechaFinalizacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idincidencia`),
  KEY `fk_incidencia_empleado1_idx` (`idempleado`),
  CONSTRAINT `fk_incidencia_empleado1` FOREIGN KEY (`idempleado`) REFERENCES `empleado` (`idempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incidencia`
--

LOCK TABLES `incidencia` WRITE;
/*!40000 ALTER TABLE `incidencia` DISABLE KEYS */;
INSERT INTO `incidencia` VALUES (2,'000001','Incidencia Test','Baja','','2015-09-01 05:00:00','HelpDesk',1,'Software','Finalizada','Problema de conexión a la red inalambrica.',1,1,'0000-00-00',NULL),(4,'000002','Titulo 2','Alta','','2015-09-02 05:00:00','HelpDesk',1,'Software','Finalizada','Atención ejemplo',1,1,'0000-00-00','2016-04-08 08:37:42'),(5,'000003','Titulo incidencia','Baja','','2016-04-07 00:13:25','HelpDesk',2,'Hardware','Pendiente','hola como esas',1,1,'2016-04-06',NULL);
/*!40000 ALTER TABLE `incidencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcion`
--

DROP TABLE IF EXISTS `opcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opcion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(100) NOT NULL,
  `submenu` varchar(100) NOT NULL,
  `estado` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcion`
--

LOCK TABLES `opcion` WRITE;
/*!40000 ALTER TABLE `opcion` DISABLE KEYS */;
INSERT INTO `opcion` VALUES (1,'Incidencias','','1'),(2,'Empleado','','1'),(3,'Usuario','','1'),(4,'Reporte','','1'),(5,'Configuración','','1');
/*!40000 ALTER TABLE `opcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcion_usuario`
--

DROP TABLE IF EXISTS `opcion_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opcion_usuario` (
  `usuario` int(11) NOT NULL,
  `opcion` int(11) NOT NULL,
  `estado` char(1) NOT NULL,
  PRIMARY KEY (`usuario`,`opcion`),
  KEY `fk_opcionUsuario_opcion_idx` (`opcion`),
  CONSTRAINT `fk_opcionUsuario_opcion` FOREIGN KEY (`opcion`) REFERENCES `opcion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_opcionUsuario_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcion_usuario`
--

LOCK TABLES `opcion_usuario` WRITE;
/*!40000 ALTER TABLE `opcion_usuario` DISABLE KEYS */;
INSERT INTO `opcion_usuario` VALUES (1,1,'1'),(1,2,'1'),(1,3,'1'),(1,4,'1'),(1,5,'1'),(2,1,'1'),(2,2,'1'),(2,3,'0'),(2,4,'0'),(2,5,'0');
/*!40000 ALTER TABLE `opcion_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planta`
--

DROP TABLE IF EXISTS `planta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planta` (
  `idplanta` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `estado` char(1) NOT NULL,
  PRIMARY KEY (`idplanta`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planta`
--

LOCK TABLES `planta` WRITE;
/*!40000 ALTER TABLE `planta` DISABLE KEYS */;
INSERT INTO `planta` VALUES (1,'Planta A','1'),(2,'Planta B','1');
/*!40000 ALTER TABLE `planta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reasignar_incidencia`
--

DROP TABLE IF EXISTS `reasignar_incidencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reasignar_incidencia` (
  `idreasignarincidencia` int(11) NOT NULL AUTO_INCREMENT,
  `idincidencia` int(11) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `empleadoasignado` int(11) DEFAULT '0' COMMENT 'empleado quien fue asignado',
  PRIMARY KEY (`idreasignarincidencia`),
  KEY `fk_historial_incidencia_incidencia1_idx` (`idincidencia`),
  CONSTRAINT `fk_historial_incidencia_incidencia1` FOREIGN KEY (`idincidencia`) REFERENCES `incidencia` (`idincidencia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reasignar_incidencia`
--

LOCK TABLES `reasignar_incidencia` WRITE;
/*!40000 ALTER TABLE `reasignar_incidencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `reasignar_incidencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `estado` char(1) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idempleado` int(11) NOT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_empleado1_idx` (`idempleado`),
  CONSTRAINT `fk_usuario_empleado1` FOREIGN KEY (`idempleado`) REFERENCES `empleado` (`idempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'admin','7c222fb2927d828af22f592134e8932480637c0d',5,'1','2015-09-01 08:03:08',1),(2,'ricoru21@gmail.com','7c222fb2927d828af22f592134e8932480637c0d',1,'1','2016-04-06 21:44:55',2);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-24 17:47:36

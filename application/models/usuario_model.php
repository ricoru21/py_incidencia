<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class usuario_model extends CI_Model {

	
	function __construct() {
		parent::__construct();
	}

	function insert($data){
		if ($this->db->insert('usuario',$data)){
			return true;
		}else{
			return false;
		}
	}

	function update($id,$data){
		$this->db->where('idusuario',$id);
		if ($this->db->update('usuario',$data)){
			return true;
		}else{
			return false;
		}
	}
	
	function get_usuario_all(){
		$query = $this->db->query("select usuario.idusuario, usuario.username, usuario.type, case usuario.type when 1 then 'HelpDesk' when 2 then 'Empleado' when 3 then 'Administrador' when 4 then 'Tecnico' end as nom_type, 
		 usuario.fecharegistro, concat(e.nombre,' ', e.apellidos) as nom_empleado,usuario.idempleado from usuario inner join empleado e on e.idempleado = usuario.idempleado where usuario.estado='1'"); //and usuario.type!=3
        return $query->result_array();
	}

	function login($data){
		$query = $this->db->query("select * from usuario where username='".$data["username"]."' and password='".$data["password"]."'");
        return $query->result_array();
	}

	function update_perfil($data,$id_usuario)
	{	
		$query = $this->db->query("select o.id, o.menu, o.submenu, pu.estado from opcion_usuario pu inner join opcion o on o.id = pu.opcion where pu.usuario =".$id_usuario);
		$accesos = $query->result_array();
		if(count($accesos)>0){
			$this->db->where('Usuario', $id_usuario);
			$this->db->update_batch('opcion_usuario', $data, 'Opcion');
		}else{
			$this->db->insert_batch('opcion_usuario', $data);
		}
	}

	public function get_accesos_byperfil($id_usuario)
	{
		$query = $this->db->query("select o.id, o.menu, o.submenu, pu.estado from opcion_usuario pu inner join opcion o on o.id = pu.opcion where pu.usuario =".$id_usuario);
		if ($this->db->trans_status() === FALSE)
		{
			return false;
		}
		else
		{	
			$accesos = $query->result_array();
			if(count($accesos)>0){
				foreach ($accesos as $key => $value) {
				$check = '<input role="checkbox" type="checkbox" class="cbox"><span class="desc"> Sin acceso</span>';
				if($value["estado"]==1)
					$check = '<input role="checkbox" type="checkbox" class="cbox" checked="checked"><span class="desc"> Con acceso</span>';

					$accesos[$key]["check"] = $check;
				}
			}else{
				$query = $this->db->query("select o.id,o.menu,o.submenu,'0' as estado from opcion o");
				$accesos = $query->result_array();
				foreach ($accesos as $key => $value) {
				$check = '<input role="checkbox" type="checkbox" class="cbox"><span class="desc"> Sin acceso</span>';
				if($value["estado"]==1)
					$check = '<input role="checkbox" type="checkbox" class="cbox" checked="checked"><span class="desc"> Con acceso</span>';

					$accesos[$key]["check"] = $check;
				}
			}
			return $accesos;
		}
	}

	public function get_login_byperfil($id_usuario)
	{
		$query = $this->db->query("select o.id, o.menu, o.submenu, pu.estado from opcion_usuario pu inner join opcion o on o.id = pu.opcion where pu.usuario =".$id_usuario);
		if ($this->db->trans_status() === FALSE)
		{
			return false;
		}
		else
		{	
			$accesos = $query->result_array();
			return $accesos;
		}
	}

	
}
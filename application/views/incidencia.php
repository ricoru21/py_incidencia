<div class="main-content">
	<div class="breadcrumbs" id="breadcrumbs">
		<script type="text/javascript">try{ace.settings.check('navbar' , 'fixed')}catch(e){}</script>
		<ul class="breadcrumb">
			<li> <i class="icon-home home-icon"></i>
				<a href="#">Home</a>
			</li>
			<li class="active">
				<a href="">Incidencia</a>
			</li>
		</ul>
	</div>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="tabbable">
					<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
						<li id="incidencia-registro" class="active">
							<a data-toggle="tab" href="#incidenciaregistro">Incidencia</a>
						</li>
						<li id="incidencia-lista">
							<a data-toggle="tab" href="#incidencialista">Consultar</a>
						</li>
					</ul>
					<div class="tab-content">
						<div id="incidenciaregistro" class="tab-pane in active">
							<div class="row">
								<div class="col-xs-12">
									<form class="form-horizontal" role="form" id="frm-buscar" name="frm-buscar">
										<div class="row">
											<div class="col-xs-12">
												<div class="form-group">
													<label class="col-sm-1 control-label no-padding-left" for="keycodeEmpleado-incidencia">Empleados</label>
													<div class="col-sm-2">
														<div class="input-group" style="cursor: pointer;">
															<input class="form-control validate[required,minSize[8],maxSize[8]" type="text" id="keycodeEmpleado-incidencia" name="keycodeEmpleado-incidencia" placeholder=" Código Empleado " maxlength="8" />
															<span class="input-group-addon" id="btn-buscar-empleado">
																<i class="fa fa-search"></i>
															</span>
														</div>
													</div>
													<div class="col-sm-3">
														<label class="col-sm-12 form-control" id="nombreapeEmpleado-incidencia">Nombre y Apellidos</label>
													</div>
													<div class="col-sm-3">
														<label class="col-sm-12 form-control" id="cargoEmpleado-incidencia">Planta/Area</label>
													</div>
													<div class="col-sm-2">
														<label class="col-sm-12 form-control" id="cargoEmpleado-cargo">Cargo</label>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="col-xs-12">
									<div class="widget-box">
										<div class="widget-body">
											<div class="widget-main">
												
												<form class="form-horizontal" role="form" id="frm-registro" name="frm-registro" action-1="<?php echo base_url();?>incidencia/registrar" action-2="<?php echo base_url();?>incidencia/actualizar" action-3="<?php echo base_url();?>incidencia/cerrar">
													<div class="row">
														<div class="col-xs-6">
															<div class="form-group">
																<label class="col-sm-4 control-label no-padding-right" for="titulo-incidencia">Titulo Incidencia</label>
																<div class="col-sm-7">
																	<input class="form-control hidden" type="text" id="id-incidencia" name="id-incidencia"/>
																	<input class="form-control validate[required,minSize[2],maxSize[30]" type="text" id="titulo-incidencia" name="titulo-incidencia" placeholder=" Titulo " />
																	<input class="form-control hidden" type="text" id="idempleado-incidencia" name="idempleado-incidencia"/>
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-4 control-label no-padding-right" for="keycode-incidencia">Código Incidencia</label>
																<div class="col-sm-7">
																	<input class="form-control validate[required,minSize[2],maxSize[30]" type="text" id="keycode-incidencia" name="keycode-incidencia" readonly />
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-4 control-label no-padding-right" for="prioridad-incidencia">Prioridad</label>
																<div class="col-sm-7">
																	<select id="prioridad-incidencia" name="prioridad-incidencia" class="form-control validate[required]">
																		<option value="Baja">Baja</option>
																		<option value="Normal">Normal</option>
																		<option value="Alta">Alta</option>
																	</select>
																</div>
															</div>
															<!--<div class="form-group">
																<label class="col-sm-4 control-label no-padding-right" for="tiemporesolucion-incidencia">Tiempo Resolución</label>
																<div class="col-sm-7">
																	<input class="form-control validate[required,minSize[2],maxSize[30]" type="text" id="tiemporesolucion-incidencia" name="tiemporesolucion-incidencia" placeholder=" Tiempo Resolución " />
																</div>
															</div>-->
															<div class="form-group">
																<label class="col-lg-4 control-label no-padding-right" for="fechaincidencia-incidencia">Fecha Incidencia</label>
																<div class="col-lg-7">
																	<div class="input-group">
																		<input type="text" id="fechaincidencia-incidencia" name="fechaincidencia-incidencia" class="form-control date-picker-edad validate[required,custom[date]]" placeholder="Fecha " class="col-xs-10 col-sm-5" readonly/>
																		<span class="input-group-addon">
																			<i class="glyphicon glyphicon-calendar bigger-110"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-xs-6">
															
															<div class="form-group">
																<label class="control-label col-lg-3 no-padding-right"for="asignado-incidencia">ASIGNADO A</label>
																<div class="col-sm-8">
																	<div class="input-group" style="cursor: pointer;">
																		<input type="text" id="idempleado-asignado" name="idempleado-asignado" class="hidden" />
																		<label class="col-sm-5 form-control" id="empleado-asignado"></label>
																		<select id="asignado-incidencia" name="asignado-incidencia" class="form-control validate[required]">
																			<option value="HelpDesk">HelpDesk</option>
																			<option value="Técnico">Técnico</option>
																			<option value="Supervisor">Supervisor</option>
																		</select>
																		<span class="input-group-addon" id="btn-buscar-empleado-asignar">
																			<i class="fa fa-search"></i>
																		</span>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-lg-3 no-padding-right"for="tipo-incidencia">TIPO</label>
																<div class="col-lg-8">
																	<select id="tipo-incidencia" name="tipo-incidencia" class="form-control validate[required]">
																		<option value="Hardware">Hardware</option>
																		<option value="Software">Sofware</option>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-lg-3 no-padding-right"for="estado-incidencia">ESTADO</label>
																<div class="col-lg-8">
																	<select id="estado-incidencia" name="estado-incidencia" class="form-control validate[required]">
																		<option value="Pendiente">Pendiente</option>
																		<option value="En Proceso">En Proceso</option>
																		<option value="Finalizada">Finalizada</option>
																	</select>
																</div>
															</div>
														</div>
														<div class="col-xs-12">
															<div class="form-group">
																<label class="control-label col-lg-2 no-padding-right"for="estado-incidencia">DESCRIPCION</label>
																<div class="col-lg-9">
																	<textarea id="descripcion-incidencia" name="descripcion-incidencia" class="form-control validate[required]" rows="4" cols="50"></textarea>
																</div>
															</div>
														</div>
													</div>
													<div class="clearfix form-actions">
														<div class="col-md-12">
															<button id="btn-cancelar-incidencia" class="btn pull-right" type="reset">
																<i class="ace-icon fa fa-undo bigger-110"></i>
																Salir
															</button>
															<button id="btn-guardar-incidencia" class="btn btn-primary" type="button">
																<i class="glyphicon glyphicon-floppy-disk bigger-110"></i>
																Guardar
															</button>
															<button id="btn-actualizar-incidencia" class="btn btn-success" type="button">
																<i class="glyphicon glyphicon-floppy-saved bigger-110"></i>
																Actualizar
															</button>
															<button id="btn-cerrar-incidencia" class="btn btn-danger" type="button">
																<i class="glyphicon glyphicon-floppy-remove bigger-110"></i>
																Cerrar Incidencia
															</button>
														</div>
													</div>
												</form>

												<!-- modales -->
												<div class="modal fade" id="modal-personal-asignar" >
											        <div class="modal-dialog" style="width:750px;">
											            <div class="modal-content">
											                <div class="modal-header">
											                    <h3>Lista de Empleados</h3>
											                </div>
											                <div class="modal-body">
											                    <div class="row">
											                        <div class="col-lg-12">
											                            <div class="box-body table-responsive">
											                                <table class="table table-striped table-bordered bootstrap-datatable datatable" 
											                                    id="tabla-empleado-asignar">
											                                    <thead>
											                                        <tr>
											                                        	<th>ID</th>
											                                            <th>Persona</th>
											                                            <th>DNI</th>
											                                        </tr>
											                                    </thead>
											                                    <tbody></tbody>
											                                </table>
											                            </div>
											                        </div>
											                    <!-- /.table-responsive -->
											                    </div>
											                </div>
											                <div class="modal-footer">
											                    <a href="#" id="btn-cancelar-modal" class="btn btn-default" data-dismiss="modal"><i class="ace-icon fa fa-undo bigger-110"></i> Salir</a>
											                    <button data-dismiss="modal" class="btn btn-primary" id="btn-seleccionar-empleo" type="button">Seleccionar</button>
											                </div>
											            </div>
											        </div>
											    </div>
												<!-- -------- -->

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="incidencialista" class="tab-pane">
							<div class="row">
								<div class="col-xs-12">
									<div class="box-header">
										<h3 class="header smaller lighter blue">Lista Incidencias</h3>
									</div>
									<div class="box-body table-responsive" >
										<table class="table table-striped table-bordered bootstrap-datatable datatable" id="incidencia_table" data-source ="<?php echo base_url();?>servicios/get_incidencia_all">
											<thead>
												<tr>
													<th>Código</th>
													<th>Titulo<br/>Incidencia</th>
													<th>Empleado<br/>Atendido</th>
													<th>Tiempo</th>
													<th>Fecha</th>
													<th>Asignado</th>
													<th>Responsable</th>
													<th>Tipo</th>
													<th>Estado</th>
													<th>Usuario<br/>Crea</th>
												</tr>
											</thead>
											<thead>
												<tr>
													<th class="input">
														<input type="text" placeholder="Código"  maxlength="8" class="search_init" />
													</th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
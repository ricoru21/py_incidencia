<!doctype html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $title ?> AC</title>

		<meta name="description" content="sistema incidencia &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<?php
			echo link_tag('assets/css/bootstrap.min.css');
			echo link_tag('assets/css/font-awesome.min.css');
			echo link_tag('assets/css/jquery-ui.custom.min.css');
			echo link_tag('assets/css/ace-fonts.css');
			echo link_tag('assets/css/ace.min.css');
			echo link_tag('assets/css/ace-skins.min.css');
			echo link_tag('assets/css/ace.onpage-help.css');
			echo link_tag('docs/assets/js/themes/sunburst.css');
		?>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/chosen.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/datepicker3.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/colorpicker.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ionicons.min.css"/>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-tagsinput.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-part2.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/datatables.actions.css" />

		<!-- ace settings handler -->
		<script src="<?php echo base_url();?>assets/js/ace-extra.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>
		<script src="<?php echo base_url();?>/assets/js/respond.min.js"></script>

		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/index.css" />

		<style type="text/css">
			.datepicker { z-index : 1151 ! important ;   }
			.bootstrap-timepicker-widget { z-index : 1151 ! important ;   }
		</style>

		<link rel="stylesheet" href="<?php echo base_url();?>
		assets/css/jqueryvalidation/css/validationEngine.jquery.css" />
	</head>

	<body class="skin-1">
		<!-- #section:basics/navbar.layout -->
		<div id="navbar" class="navbar navbar-default navbar-collapse h-navbar" id="InicioIntro">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				
				<div class="navbar-header pull-left" >
					<!-- #section:basics/navbar.layout.brand -->
					<a href="#" target="_blank" class="navbar-brand">
						<small>
							SISTEMA INCIDENCIAS
						</small>
					</a>
					<!-- /section:basics/navbar.layout.brand -->

					<!-- #section:basics/navbar.toggle -->
					<button class="pull-right navbar-toggle navbar-toggle-img collapsed" type="button" data-toggle="collapse" data-target=".navbar-buttons,.navbar-menu">
						<span class="sr-only">Toggle user menu</span>
						<img src="<?php echo base_url();?>assets/avatars/user.jpg" alt="Usuario" />
					</button>
					
					<!-- #section:basics/sidebar.mobile.toggle -->
					<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler">
						<span class="sr-only">Toggle sidebar</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

				</div>

				<!-- #section:basics/navbar.dropdown -->
				<div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
					<ul class="nav ace-nav">
						<li class="grey red user-min" id="Perfil">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<span class="user-info">
									<small></small>
									<?php 
										if(isset($this->session->userdata('persona')[0]))
										{ 
									     	echo strtoupper($this->session->userdata('persona')[0]["username"]);
									 	} 
									?>
								</span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close" >
								<li>
									<a href="<?php echo base_url();?>login/logout">
										<i class="ace-icon fa fa-power-off"></i>Salir
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>

			</div><!-- /.navbar-container -->
		</div>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>SISTEMA INCIDENCIAS</title>

		<meta name="description" content="Login Adminchurch" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-fonts.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jqueryvalidation/css/validationEngine.jquery.css" />

	</head>

	<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center" style="margin-top: 200px;">							
								
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box">
									<div class="widget-body" style="border-radius:5px">
										<div class="widget-main">
											<div class="row">
												<div class="col-md-12">
													<h3 style="text-align:center; color: #f74e4d;">	
													<br>
														Iniciar de Sesión
													</h3>
												</div>
											</div>
											<div class="space-6"></div>

											<form class="form-horizontal" method="post" action="<?php echo base_url();?>login/access">

												<?php if(isset($message) != ""): ?>
									            		<div class="alert alert-info">
									                	<?php echo $message;?></div>
									            <?php endif ?>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" id="username" name="username" 
															class="form-control" placeholder="" />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" id="password" name="password" 
															maxlength="18" class="form-control" placeholder="" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">

														<button type="submit" class="width-35 pull-right btn btn-danger btn-sm">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Acceder</span>
														</button>

													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

							</div><!-- /.position-relative -->

							<div class="navbar-fixed-top align-right"></div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
				<!-- MODAL---- -->
				<div class="modal fade" id="okusuario">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">
									<i class="icon-exclamation-triangle"></i>
									Atención!
								</h4>
							</div>
							<div class="modal-body">
								<div class="alert alert-danger alert-dismissable">
									<p>Registro Correctamente</p>
								</div>
							</div>
							<div class="modal-footer clearfix">
								<a href="#" id="hrefmodal_ok" class="btn" data-dismiss="modal">Aceptar</a>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="errorusuario">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title"><i class="icon-exclamation-triangle"></i> Atención! </h4>
							</div>
							<div class="modal-body">
								<div class="alert alert-danger alert-dismissable">
									<p>
										Incorrecto
									</p>
								</div>
							</div>
							<div class="modal-footer clearfix">
								<a href="#" class="btn" data-dismiss="modal">Aceptar</a>
							</div>
						</div>
		        	</div>
				</div>

			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo base_url();?>assets/js/jquery.min.js'>"+"<"+"/script>");
		</script>

		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url();?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>

		<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/util/functiongen.js"></script>
		<script src="<?php echo base_url();?>assets/js/jqueryvalidation/languages/jquery.validationEngine-es.js"></script>
		<script src="<?php echo base_url();?>assets/js/jqueryvalidation/jquery.validationEngine.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.blockUI.js"></script>
		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			
			jQuery(function($) {

				$(document).on('click', '.toolbar a[data-target]', function(e) {
					e.preventDefault();
					var target = $(this).data('target');
					$('.widget-box.visible').removeClass('visible');//hide others
					$(target).addClass('visible');//show target
				});
			
			});

		</script>
	</body>
</html>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class servicios extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('configuracion_model','confmod');
	}

	public function get_planta_all()
	{		
		$result = $this->confmod->get_planta_all();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}

	public function get_area_all()
	{		
		$result = $this->confmod->get_area_all();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}

	public function get_cargo_all()
	{	
		$result = $this->confmod->get_cargo_all();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}


	public function get_area_all_byIdPlanta()
	{	
		$idplanta = $this->input->post("idplanta");
		$result = $this->confmod->get_area_all_byIdPlanta($idplanta);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}

	public function get_empleado_all(){
		$this->load->model('empleado_model','empmod');
		$result = $this->empmod->get_empleado_all();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}

	public function obtenerEmpleado_keyCode(){
		$keycode = $this->input->post("keycode_empleado");
		$this->load->model('empleado_model','empmod');
		$result = $this->empmod->obtenerEmpleado_keyCode($keycode);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}

	public function get_usuario_all(){
		$this->load->model('usuario_model','usumod');
		$result = $this->usumod->get_usuario_all();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}

	public function get_incidencia_all(){
		$this->load->model('incidencia_model','incmod');
		$result = $this->incmod->get_incidencia_all();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}

	public function get_empleados_asignar_byTipoAsignacion($tipo){
		$this->load->model('empleado_model','empmod');
		$result = $this->empmod->get_empleados_asignar_byTipoAsignacion($tipo);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}

	public function get_reporte_byparam($fecha,$area,$mes,$anio,$estado){
		$this->load->model('reporte_model','mod');
		$result = $this->mod->get_reporte_byparam($fecha,$area,$mes,$anio,$estado);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}


}
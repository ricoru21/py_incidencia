<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {

	public function index()
	{
		
		if($this->session->flashdata('message')!=""){
			$this->data['message'] = $this->session->flashdata('message');
			$this->load->view('home',$this->data);	
		}else{
			$this->load->view('home');	
		}
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
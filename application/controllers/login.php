<?php defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	//redirect if needed, otherwise display the user list
	public function access()
	{

		if(isset($_POST['username']) && isset($_POST['password'])){
			$this->load->model('usuario_model','mod');

			$data = array(
				'username' =>$_POST['username'],
				'password' =>sha1($_POST['password'])
				//'password' =>$_POST['password']
			);

			$return = $this->mod->login($data);
			
			if(count($return)==0){
				$this->session->set_flashdata('message', "El usuario o contraseña no son válidos");
				redirect('/home', 'refresh');
			}else{
				$persona = $return;				
				$this->session->set_userdata('persona',$persona);

				$this->load->model('usuario_model','umod');
				$perfiles = $this->umod->get_login_byperfil($persona[0]['idusuario']);
				$this->session->set_userdata('perfiles',$perfiles);
				
				redirect('/principal', 'refresh');
			}
		}else{
			$this->session->set_flashdata('message', "El usuario o contraseña no son válidos");
			redirect('/home', 'refresh');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('persona');
		redirect('/home', 'refresh');
	}

	function _render_page($view, $data=null, $render=false)
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $render);

		if (!$render) return $view_html;
	}

}

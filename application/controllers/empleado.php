<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class empleado extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//if (!$this->ion_auth->logged_in())
		//redirect('home', 'refresh');
	}

	public function registrar(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('empleado_model','emodel');

			$keycode = $form["keycode-empleado"];
			$dni = $form["dni-empleado"];
			$nombre = $form["nombre-empleado"];
			$apellidos = $form["apellidos-empleado"];
			
			$fechanac = $form['fechanac-empleado']==""?date("d/m/Y"):$form['fechanac-empleado'] ;
			$fechanac = date_create_from_format("d/m/Y",$fechanac);
			$fechanac = $fechanac->format('Y-m-d');

			$area = $form["area-empleado"];
			$cargo = $form["cargo-empleado"];
			$anexo = $form["anexo-empleado"];
			$email = $form["email-empleado"];
			
			$data = array('keycode' =>$keycode, 
						  'dni' =>$dni,
						  'nombre' =>$nombre,
						  'apellidos' =>$apellidos,
						  'fechanacimiento' =>$fechanac,
						  'estado' =>'1',
						  'anexo' =>$anexo,
						  'correo' =>$email,
						  'idarea' =>$area,
						  'idcargo' =>$cargo
						  );
			
			if($this->emodel->insert($data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			};

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

	public function actualizar(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('empleado_model','emodel');

			$id = $form["id-empleado"];
			$keycode = $form["keycode-empleado"];
			$dni = $form["dni-empleado"];
			$nombre = $form["nombre-empleado"];
			$apellidos = $form["apellidos-empleado"];
			
			$fechanac = $form['fechanac-empleado']==""?date("d/m/Y"):$form['fechanac-empleado'] ;
			$fechanac = date_create_from_format("d/m/Y",$fechanac);
			$fechanac = $fechanac->format('Y-m-d');

			$area = $form["area-empleado"];
			$cargo = $form["cargo-empleado"];
			$anexo = $form["anexo-empleado"];
			$email = $form["email-empleado"];
			
			$data = array('keycode' =>$keycode, 
						  'dni' =>$dni,
						  'nombre' =>$nombre,
						  'apellidos' =>$apellidos,
						  'fechanacimiento' =>$fechanac,
						  'estado' =>'1',
						  'anexo' =>$anexo,
						  'correo' =>$email,
						  'idarea' =>$area,
						  'idcargo' =>$cargo
						  );
			
			if($this->emodel->update($id,$data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}; 

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

	public function obtenerkeyCode(){
		$this->load->model('empleado_model','emodel');
		$result = $this->emodel->obtenerkeyCode();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}
	
}
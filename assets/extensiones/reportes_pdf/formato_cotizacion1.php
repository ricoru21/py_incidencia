<?php
$title = $_POST['title'];
$table_resumen = $_POST['table_resumen'];
$table_reporte = $_POST['table_reporte'];

ob_start();
?>
    
    <style type="text/css">
	<!--
	table{
		width: 100%;
	}
	th{
		background: #e7e6e6;
	}
	#header{
		width: 100%;
	}
	#logo{
		width:20%;
		padding: 8px;
	}

	#divh3{
		text-align: center;
		text-transform: uppercase;
		width: 100%;
	}
	
	#resume, #total{
		border: #111 1px solid;
		padding: 10px;
	}
	#resume td.impar, .upbold{
		font-weight: bold; 
		text-transform: uppercase;
	}
	#treporte{
		border-collapse: collapse;
		text-align: center;
	}
	#treporte td.head{
		background: #111;
		color: #fff;
		text-transform: uppercase;
	}

	#treporte td.herramientas{
		background: #111;
		color: #fff;
		width: 3%;
		text-transform: uppercase;
		text-align:center;
	}

	#treporte th, #treporte td{
		border: black 1px solid;
	}
	#treporte td.izquierda{
		text-align: left;
		padding-left: 17px;
	}
	.upbold{
		text-align: right;
	}

	</style>
	
	<page backtop="1mm" backbottom="9mm" backleft="1mm" backright="1mm">
		<page_footer>
			<div>
				<hr>
				<span> Página [[page_cu]]/[[page_nb]]</span>
			</div>
	    </page_footer>
		<table id="header">
			<tr>
				<td style="height: 5px;"></td>
			</tr>
			<tr>
				<td id="divh3">
					<h3><?php echo $title ?></h3>
				</td>
			</tr>
		</table>
		<br>
		<br>
		<?php echo $table_resumen ?>
		<br>
		<?php echo $table_reporte ?>
    </page>
    <?php 
  	$content = ob_get_clean();

    require_once(dirname(__FILE__).'/../html2pdf/html2pdf.class.php');
    $html2pdf = new HTML2PDF('L','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content);
    $html2pdf->Output('reporte_actividades_'.date("d-m-Y").'.pdf');
?>

<!--
<?php
$title = $_POST['title'];
$table_resumen = $_POST['table_resumen'];
$table_reporte = $_POST['table_reporte'];

ob_start();
?>
    
    <style type="text/css">
	<!--
	table{
		width: 100%;
	}
	th{
		background: #e7e6e6;
	}
	#header{
		width: 100%;
	}
	#logo{
		width:20%;
		padding: 8px;
	}

	#divh3{
		background: #111;
		color: #fff;
		padding-right: 15px;
		text-align: right;
		text-transform: uppercase;
		width: 80%;
	}
	
	#resume, #total{
		border: #111 1px solid;
		padding: 10px;
	}
	#resume td.impar, .upbold{
		font-weight: bold; 
		text-transform: uppercase;
	}
	#treporte{
		border-collapse: collapse;
		text-align: center;
	}
	#treporte td.head{
		background: #111;
		color: #fff;
		text-transform: uppercase;
	}

	#treporte td.herramientas{
		background: #111;
		color: #fff;
		width: 3%;
		text-transform: uppercase;
		text-align:center;
	}

	#treporte th, #treporte td{
		border: black 1px solid;
	}
	#treporte td.izquierda{
		text-align: left;
		padding-left: 17px;
	}
	.upbold{
		text-align: right;
	}

	</style>
	
	<page backtop="1mm" backbottom="9mm" backleft="1mm" backright="1mm">
		<page_footer>
			<div>
				<hr>
				<span> Página [[page_cu]]/[[page_nb]]</span>
			</div>
	    </page_footer>
		<table id="header">
			<tr>
				<td id="logo" rowspan="3">
					<img alt="" src="../../img/siradG2.png">
				</td>
			</tr>
			<tr></tr>
			<tr>
				<td style="height: 20px;"></td>
			</tr>
			<tr>
				<td></td>
				<td id="divh3">
					<h3><?php echo $title ?></h3>
				</td>
			</tr>
			<tr>
				<td style="height: 20px;"></td>
			</tr>
		</table>
		<br>
		<br>
		<?php echo $table_resumen ?>
		<br>
		<?php echo $table_reporte ?>
    </page>
    <?php 
  	$content = ob_get_clean();

    require_once(dirname(__FILE__).'/../html2pdf/html2pdf.class.php');
    $html2pdf = new HTML2PDF('L','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content);
    $html2pdf->Output('reporte_actividades_'.date("d-m-Y").'.pdf');
?>
-->